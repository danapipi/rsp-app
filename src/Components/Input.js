import React, { useRef, useEffect, useState } from 'react'
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { CachedImage } from 'react-native-cached-image'

const Input = ({
  label,
  labelColor,
  labelMarginBottom,
  placeHolder,
  labelFontSize,
  labelFontFamily,
  showPassword,
  boxColor,
  isPassword,
  changeShowPassword,
  changeValue,
  valueInput,
  error,
  errorMessage,
  textColor,
  widthBox,
  containerMarginBottom,
  containerBgcolor,
  refInputName,
  appiumLabel,
  appiumLabelPassword,
  disableEdit,
  textInfo,
  textInfoColor,
  ...props
}) => {
  let refInput = useRef(() => refInputName)
  const [isFocused, setFocus] = useState(false)

  const onFocus = () => {
    setFocus(true)
  }
  const onBlur = () => {
    setFocus(false)
  }

  const prevFocusRef = useRef()
  useEffect(() => {
    console.log(appiumLabel, 'useEffect =')
    prevFocusRef.current = isFocused
  }, [])

  const prevFocus = prevFocusRef.current

  return (
    <View
      style={{
        marginHorizontal: moderateScale(16),
        marginBottom: moderateScale(containerMarginBottom) || moderateScale(24),
        backgroundColor: containerBgcolor,
      }}
    >
      <Text
        style={{
          marginBottom:
            moderateScale(labelMarginBottom, 0.25) || moderateScale(4, 0.25),
          fontSize: moderateScale(labelFontSize) || moderateScale(12, 0.25),
          fontFamily: labelFontFamily || 'PublicSans-Regular',
          color: disableEdit ? '#454849' : labelColor || '#838586',
        }}
      >
        {label}
      </Text>
      <View
        style={{
          backgroundColor: disableEdit ? '#212325FC' : boxColor || '#454849FC',
          height: moderateScale(42, 0.25),
          width: widthBox || '100%',
          justifyContent: 'center',
          flexDirection: 'row',
          borderRadius: 2,
          borderColor:
            error && !isFocused
              ? '#FF5252'
              : isFocused
              ? '#55ADFF'
              : '#454849FC',
          borderWidth: error || isFocused ? 1 : 0,
          marginBottom: moderateScale(4),
        }}
      >
        {disableEdit ? (
          <Text
            style={{
              marginHorizontal: moderateScale(16),
              flex: isPassword ? 7 : 1,
              fontSize: valueInput || moderateScale(14),
              fontFamily: labelFontFamily || 'PublicSans-Regular',
              color: textColor || '#454849',
              alignSelf: 'center',
            }}
            placeholder={placeHolder || 'Enter your ....'}
            placeholderTextColor={'#838586'}
            // secureTextEntry={!showPassword || false}
            // onChangeText={changeValue}
            // ref={refInput}
            // onSubmitEditing={() => refInput.current.focus()}
            // selectionColor={"#1F93FF"}
            accessibilityLabel={appiumLabel}
            testID={appiumLabel}
            {...props}
          >
            {props.value}
          </Text>
        ) : (
          <TextInput
            style={{
              marginHorizontal: moderateScale(16),
              flex: isPassword ? 7 : 1,
              fontSize: valueInput || moderateScale(14),
              fontFamily: labelFontFamily || 'PublicSans-Regular',
              color: textColor || '#FFFFFF',
            }}
            placeholder={placeHolder || 'Enter your ....'}
            placeholderTextColor={'#838586'}
            // secureTextEntry={!showPassword || false}
            onChangeText={changeValue}
            ref={refInput}
            onSubmitEditing={() => refInput.current.focus()}
            selectionColor={'#1F93FF'}
            accessibilityLabel={appiumLabel}
            testID={appiumLabel}
            onFocus={onFocus}
            onBlur={onBlur}
            {...props}
            // selectionColor={'#1F93FF'}
          />
        )}

        {isPassword && (
          <TouchableOpacity
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={changeShowPassword}
            accessibilityLabel={appiumLabelPassword}
            testID={appiumLabelPassword}
          >
            {showPassword ? (
              <CachedImage
                style={{
                  width: moderateScale(16),
                  height: moderateScale(11),
                }}
                source={require('../assets/img/eyeShow.png')}
              />
            ) : (
              <CachedImage
                style={{
                  width: moderateScale(16),
                  height: moderateScale(11),
                }}
                source={require('../assets/img/eyeNotShow.png')}
              />
            )}
            {/* <Icon
              name={showPassword ? "eye" : "eye-slash"}
              size={moderateScale(18)}
              color="#333333"
            /> */}
          </TouchableOpacity>
        )}
      </View>
      {appiumLabel === 'profileEditName' && disableEdit ? (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text
            style={{
              color: `${textInfoColor}`,
              fontSize: moderateScale(10),
              fontFamily: 'PublicSans-Regular',
              // marginLeft: moderateScale(4),
            }}
          >
            {textInfo}
          </Text>
        </View>
      ) : (
        error && (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <CachedImage
              style={{
                width: moderateScale(11),
                height: moderateScale(11),
              }}
              source={require('../assets/img/wrongIcon.png')}
            />
            <Text
              style={{
                color: '#FF5252',
                fontSize: moderateScale(10),
                fontFamily: 'PublicSans-Regular',
                marginLeft: moderateScale(4),
              }}
            >
              {errorMessage}
            </Text>
          </View>
        )
      )}
    </View>
  )
}

export default Input
